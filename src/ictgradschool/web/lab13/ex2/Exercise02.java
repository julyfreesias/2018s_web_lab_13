package ictgradschool.web.lab13.ex2;

import ictgradschool.web.lab13.Keyboard;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class Exercise02 {
    public static void main(String[] args) {
        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        Properties dbProps = new Properties();

        try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Welcome to the Film database!:");
        System.out.println(" ");

        outerloop:
        while (true) {
            System.out.println("Please select an option from the following:");
            System.out.println("1. Information by Actor");
            System.out.println("2. Information by Movie");
            System.out.println("3. Information by Genre");
            System.out.println("4. Exit");
            String userInput = Keyboard.readInput();
            int index = Integer.parseInt(userInput);


            switch (index) {
                case 1:
                    handleActor(dbProps);
                    break;
                case 2:
                    handleMovie(dbProps);
                    break;
                case 3:
                    handleGenre(dbProps);
                    break;
                case 4:
                    break outerloop;
                default:
                    break;
            }

        }

    }

    private static void handleGenre(Properties dbProps) {
        System.out.println("Please enter the name of the actor you wish to " +
                "get information about, or press enter to return " +
                "to the previous menu");

        while (true) {

            String userInput = Keyboard.readInput();

            if (userInput.isEmpty()) {
                break;
            }

            // Set the database name to your database
            try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {

                try (PreparedStatement stmt = conn.prepareStatement("SELECT PF.film_title,PG.genre_name\n" +
                        "FROM \n" +
                        "\n" +
                        "pfilms_film AS PF,\n" +
                        "pfilms_role AS PR,\n" +
                        "pfilms_genre AS PG\n" +
                        "WHERE \n" +
                        "PG.genre_name= ?\n" +
                        "AND\n" +
                        "PG.genre_name = PF.genre_name\n" +
                        "\n")) {
                    stmt.setString(1, userInput); //? means input parameter


                    try (ResultSet r = stmt.executeQuery()) {

                        if (r.next() == false) {
                            System.out.println("Sorry, we couldn't find any genre by that name.");
                        } else {


//                            String genre = r.getString(1);
//                            String film_title = r.getString(2);


                            System.out.println("The " + userInput + " genre includes the following films:");


                            while (r.next()) {
                                String genre = r.getString(1);
                                String film_title = r.getString(2);
                                System.out.println("The " + userInput + " genre includes the following films:");
                            }
                        }

                    }
                }

                System.out.println("Please enter the name of the actor you wish to " +
                        "get information about, or press enter to return " +
                        "to the previous menu");

            } catch (SQLException e) {
                e.printStackTrace();


            }
        }
    }


    private static void handleMovie(Properties dbProps) {
        System.out.println("Please enter the name of the actor you wish to " +
                "get information about, or press enter to return " +
                "to the previous menu");

        while (true) {

            String userInput = Keyboard.readInput();

            if (userInput.isEmpty()) {
                break;
            }

            // Set the database name to your database
            try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {

                try (PreparedStatement stmt = conn.prepareStatement("SELECT PA.actor_fname, PA.actor_lname, PF.film_title, PR.role_name \n" +
                        "FROM \n" +
                        "\n" +
                        "pfilms_participates_in AS PPI,\n" +
                        "pfilms_film AS PF,\n" +
                        "pfilms_role AS PR,\n" +
                        "pfilms_actor AS PA\n" +
                        "WHERE \n" +
                        "PF.film_title= ?\n" +
                        "AND\n" +
                        "PF.film_id = PPI.film_id\n" +
                        "AND\n" +
                        "PPI.film_id = PF.film_id\n" +
                        "AND\n" +
                        "PPI.actor_id = PA.actor_id\n")) {
                    stmt.setString(1, userInput); //? means input parameter


                    try (ResultSet r = stmt.executeQuery()) {

                        if (r.next() == false) {
                            System.out.println("Sorry, we couldn't find any film by that name.");
                        } else {



                            String genre = r.getString(1);

                            r.beforeFirst();

                            System.out.println("The film " + userInput + " is a " + genre + "that features the" +
                                    "following people:");

                            while (r.next()) {
                                String fname = r.getString(2);
                                String lname = r.getString(3);
                                String roleName = r.getString(4);
                                System.out.println(fname + "" + lname + " (" + roleName + ")");
                            }
                        }

                    }
                }

                System.out.println("Please enter the name of the actor you wish to " +
                        "get information about, or press enter to return " +
                        "to the previous menu");

            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

    private static void handleActor(Properties dbProps) {

        System.out.println("Please enter the name of the actor you wish to " +
                "get information about, or press enter to return " +
                "to the previous menu");

        while (true) {

            String userInput = Keyboard.readInput();

            if (userInput.isEmpty()) {
                break;
            }

            // Set the database name to your database
            try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {

                try (PreparedStatement stmt = conn.prepareStatement("SELECT PA.actor_fname, PA.actor_lname, PF.film_title, PR.role_name \n" +
                        "FROM \n" +
                        "pfilms_actor AS PA ,\n" +
                        "pfilms_participates_in AS PPI,\n" +
                        "pfilms_film AS PF,\n" +
                        "pfilms_role AS PR\n" +
                        "WHERE \n" +
                        "(PA.actor_fname = ? OR PA.actor_lname = ? ) AND\n" +
                        "PPI.actor_id = PA.actor_id\n" +
                        "AND\n" +
                        "PPI.film_id = PF.film_id\n" +
                        "AND\n" +
                        "PPI.role_id = PR.role_id")) {
                    stmt.setString(1, userInput);
                    stmt.setString(2, userInput);


                    try (ResultSet r = stmt.executeQuery()) {

                        if (r.next() == false) {
                            System.out.println("Sorry, we couldn't find any actor by that name.");
                        } else {



                            String fname = r.getString(1);
                            String lname = r.getString(2);

                            r.beforeFirst();

                            System.out.println(fname + " " + lname + "is listed as being involved in the\n" +
                                    "following films:");

                            while (r.next()) {
                                String movieName = r.getString(3);
                                String roleName = r.getString(4);
                                System.out.println(movieName + " (" + roleName + ")");
                            }
                        }

                    }
                }
//
//                    System.out.println("Please enter the name of the actor you wish to " +
//                            "get information about, or press enter to return " +
//                            "to the previous menu");

            } catch (SQLException e) {
                e.printStackTrace();
            }


        }
    }
}


