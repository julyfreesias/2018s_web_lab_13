package ictgradschool.web.lab13.ex1;

import ictgradschool.web.lab13.Keyboard;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class Exercise01 {
    public static void main(String[] args) {
        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        Properties dbProps = new Properties();

        try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String userInput = "";
        while (userInput != null) {
            System.out.println("Enter a partial title of an article:");
            userInput = Keyboard.readInput();

            // Set the database name to your database
            try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
                System.out.println("Connection successful");
                //Anything that requires the use of the connection should be in here...
                try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM lab13_articles WHERE title LIKE ?")) {
                    stmt.setString(1, "%" + userInput + "%");

                        try(ResultSet r = stmt.executeQuery()){

                            while (r.next()){
                                String body = r.getString(3);
                                System.out.println(body);
                            }

                        }
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }
}
